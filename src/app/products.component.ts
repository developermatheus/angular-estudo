import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ProductsSerice } from './products.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
})
export class ProductsComponent implements OnInit {
  isDisabled = true;
  products = [];
  productName: string;
  private productsSubscription: Subscription;
  constructor(
    private productsService: ProductsSerice
  ) {
    setTimeout(() => {
    }, 3000);
  }
  
  ngOnInit() {
    this.products = this.productsService.getProducts();
    this.productsSubscription = this.productsService.productsUpdated.subscribe(() => {
      this.products = this.productsService.getProducts();
    });
  }

  ngOnDestroy() {
    this.productsSubscription.unsubscribe();
  }

  onAddProduct(form) {
    console.log(form);
    if(form.valid) {
      this.productsService.addProduct(form.value.productName);
    } else {
      alert("Errou!");
    }
  }
  
  // onRemoveProduct(productName: string) {
  //   this.products = this.products.filter(p => p !== productName);
  // }

}
